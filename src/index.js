import React from 'react';
import ReactDOM from 'react-dom';
import {createStore, applyMiddleware} from "redux";
import { BookStoreServiceProvider } from "./component/BookStoreServiceCenterContext/BookStoreServiceCenterContext";
import { Provider } from "react-redux";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import {logger} from "redux-logger";

import {routes} from './modules/routes/Routes'

    import reducer from "./modules/reducers/reducer";
import ErrorBoundary from "./component/ErrorBoundary/ErrorBoundary";
import BookStoreServices from "./modules/services/bookstore-services";

const bookStoreService = new BookStoreServices()

const store = createStore(reducer, applyMiddleware(logger))


const WrapperRouter = () => {
    return(
        <Router>
            <Switch>
                {routes.map( (route) => (
                    <Route exact={route.exact} key={route.path} {...route} />
                )) }
            </Switch>
        </Router>
    )
}

ReactDOM.render(
    <Provider store={store}>
        <ErrorBoundary>
            <BookStoreServiceProvider value={bookStoreService}>
                <WrapperRouter />
            </BookStoreServiceProvider>
        </ErrorBoundary>
    </Provider>,

  document.getElementById('root')
);
