import React from 'react'
import BookList from "../../component/BookList/BookList";
import ShopCartTable from "../../component/ShopCartTable/ShopCartTable";
import ShopHeader from "../../component/ShopHeader/ShopHeader";

const Dashboard = () => {
    return (
        <div className="Dashboard">
            <ShopHeader/>
            <BookList/>
            <ShopCartTable/>
        </div>
    );
}
export default Dashboard;

