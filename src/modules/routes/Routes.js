import {Dashboard, Cart} from "../../page";

export const routes = [
    {
        path: '/',
        component: Dashboard,
        exact: true
    },
    {
        path: '/cart',
        component: Cart,
        exact: true
    }
]