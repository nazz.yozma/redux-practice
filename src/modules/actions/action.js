import {
    FETCH_BOOKS_LOADED,
    FETCH_BOOKS_REQUEST,
    FETCH_BOOKS_ERROR,
    BOOK_ADDED_TO_CART,
    BOOK_REMOVE_FROM_CART,
    ALL_BOOK_REMOVE_FROM_CART
} from "./types";

const bookLoaded = (listBook) => {
    return{
        type: FETCH_BOOKS_LOADED,
        payload: listBook
    }
}


const booksRequested = () => {
    return{
        type: FETCH_BOOKS_REQUEST
    }
}

const booksError = (error) => {
    return {
        type: FETCH_BOOKS_ERROR,
        payload: error
    }
}

const fetchBooks = (bookStoreService, dispatch) => () => {
    dispatch(booksRequested())
    
    bookStoreService.getBooks()
        .then( data => dispatch(bookLoaded(data)) )
        .catch((err) => dispatch(booksError(err)))
    console.log('Fetching books')
}

const bookAddedToCart = (bookId) => {
    return {
        type: BOOK_ADDED_TO_CART,
        payload: bookId
    }
}
const bookRemoveFromCart = (bookId) => {
    return{
        type: BOOK_REMOVE_FROM_CART,
        payload: bookId
    }
}
const allBookRemoveFromCart = (bookId) => {
    return{
        type: ALL_BOOK_REMOVE_FROM_CART,
        payload: bookId
    }
}




export { fetchBooks, bookAddedToCart, bookRemoveFromCart, allBookRemoveFromCart };