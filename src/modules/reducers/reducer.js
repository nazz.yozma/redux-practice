import updateShoppingCart from "./ShopCartReducer";
import updateBookList from "./BookListReducer";

const reducer = (state, action) => {
    return {
        bookList: updateBookList(state, action),
        shoppingCart: updateShoppingCart(state, action)
    }
}

 export default reducer;