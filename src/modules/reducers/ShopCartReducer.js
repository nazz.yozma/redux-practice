import {ALL_BOOK_REMOVE_FROM_CART, BOOK_ADDED_TO_CART, BOOK_REMOVE_FROM_CART} from "../actions/types";

const updateCartItems = (carItems, item, index) => {
    //когда мы отнимаем к-во елементов и оно будет равно нулю то мы делаем проверку
    if(item.count === 0) return [ ...carItems.slice(0, index), ...carItems.slice(index +1) ]

    if(index === -1) return [ ...carItems, item ]

    return [ ...carItems.slice(0, index), item, ...carItems.slice(index + 1) ]
}

const updateCartItem = (book, item = {}, quantity) => {
    const { id = book.id, count = 0, title = book.title, total = 0 } = item;

    return { id, title, count: count + quantity, total: total + quantity*book.price};
};

const updateOrder = (state, bookID, quantity) => {
    const {bookList: {books}, shoppingCart: {cartItems}} = state

    const book = books.find( ({id}) => id === bookID )
    const bookIndex = cartItems.findIndex(({id}) => id === bookID)
    const item = cartItems[bookIndex]
    const newItem = updateCartItem(book, item, quantity)

    return { orderTotal: 0, cartItems: updateCartItems(cartItems, newItem, bookIndex)}

}

const updateShoppingCart = (state, action) => {

    if (state === undefined) {
        return { cartItems: [], orderTotal: null }
    }

    switch (action.type) {
        case BOOK_ADDED_TO_CART:
            return updateOrder(state, action.payload, 1)

        case BOOK_REMOVE_FROM_CART:
            return updateOrder(state, action.payload, -1)

        case ALL_BOOK_REMOVE_FROM_CART:
            const item = state.shoppingCart.cartItems.find( ({id}) => id === action.payload)
            return updateOrder(state, action.payload, -item.count)

        default:
            return state.shoppingCart
    }
}

export default updateShoppingCart;