import React, { useEffect } from 'react'
import BookListItem from "./BookListItem/BookListItem";
import { connect } from "react-redux";
import { withBookService }  from '../HOC/WithBookService'

import './BookList.css'
import Spinner from "../Spinner/Spinner";
import ErrorIndicator from "../ErrorIndicator/ErrorIndicator";
import { fetchBooks, bookAddedToCart } from "../../modules/actions/action";


const BookList = ({fetchBooks, books, loading , error, onAddedToCart}) => {

    // useEffect(() => {
    //     axios.get(data_url )
    //         .then( res => {
    //             const data = JSON.stringify()
    //             // bookLoaded(res.data);
    //             console.log(res.data)
    //         })
    //
    // }, [bookLoaded])

    useEffect( () => fetchBooks(), [fetchBooks])

    return (
        <div className="container">
            <div className="BookList">
                <ul>
                    { error
                        ? <ErrorIndicator/>
                        : loading
                        ? <Spinner/>
                        : books.map( (book) => (
                            <li key={book.id}>
                                <BookListItem
                                    books={book}
                                    onAddedToCart={() => onAddedToCart(book.id)}
                                />
                            </li>
                        ))
                    }
                </ul>
            </div>
        </div>
    );
}

const mapStateToProps = ({bookList: { books, loading, error }} ) => {
    return{ books, loading, error }
}

// const mapDispatchToProps = { bookLoaded , booksRequested ,booksError}
const mapDispatchToProps = (dispatch, { bookStoreService } ) => {
    return{
        fetchBooks: fetchBooks(bookStoreService, dispatch),
        onAddedToCart: (id) => dispatch(bookAddedToCart(id))
    }
}


export default withBookService()(connect(mapStateToProps, mapDispatchToProps)(BookList))



