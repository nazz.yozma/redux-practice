import React from 'react'
import './BookListItem.css'
import {Link} from "react-router-dom";

const BookListItem = ({ books, onAddedToCart }) => {
    const { title, author, price, coverImage } = books;

    return (
        <div className="BookList-Item">
            <div className="BookList-Item__Cover">
                <img src={coverImage} alt="cover" />
            </div>
            <div className="BookList-Item__Details">
                <Link to="#" className="Title">{title}</Link>
                <div className="Author">{author}</div>
                <div className="Price">${price}</div>
                <button
                    onClick={onAddedToCart}
                    className="btn btn-info add-to-cart"
                >
                    Add to cart</button>
            </div>
        </div>
    );
}
export default BookListItem;

