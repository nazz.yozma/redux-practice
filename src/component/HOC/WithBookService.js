import React from "react";
import { BookStoreServiceConsumer } from "../BookStoreServiceCenterContext/BookStoreServiceCenterContext";

export const withBookService = () => (Wrapped) => {
    return (props) => {
        return(
            <BookStoreServiceConsumer>
                { (BookStoreService) => {
                    return( <Wrapped {...props}  bookStoreService={BookStoreService} /> )
                }}
            </BookStoreServiceConsumer>
        )
    }
}
