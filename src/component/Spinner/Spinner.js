import React from "react";
import './Spinner.css'

const Spinner = () => {

    return (
        <div className="container">
            <h3>Loading ...</h3>
        </div>
    )
}
export default Spinner