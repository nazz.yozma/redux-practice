import React from 'react'
import { useHistory } from 'react-router-dom'
import './ShopHeader.css'
import {Link} from "react-router-dom";


const ShopHeader  = () => {
    return(
        <div className="container">
            <header className="ShopHeader">
                <Link
                    to={''}
                    className="Logo text-dark"

                >ReStore</Link>
                <Link
                    to='/cart'
                    className="ShopHeader-Cart"
                >
                    <i className="cart-icon fa fa-shopping-cart" />
                    {/*{numItems} items (${total})*/}

                </Link>
            </header>
        </div>
    )
}

export default ShopHeader;